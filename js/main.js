const nav = document.getElementById("header-nav");
const navMenu = document.getElementById("header-menu");
const navFirstLink = document.getElementById("menu-emitter");
let isMenuActive = false;
let isMouseOnMenu = false;
let isDesktop = window.innerWidth >= 1440;


navFirstLink.onmouseover = () => {

  if (isMenuActive) return;

  if (!isMenuActive) {
    navMenu.classList.add("active");
    
  }

  isMenuActive = true;
};

navFirstLink.onmouseleave = () => {
  setTimeout(() => {
    if (!isMenuActive || isMouseOnMenu) return;
    navMenu.classList.remove("active");
    isMouseOnMenu = false;
    isMenuActive = false;
  }, 66);
};

navMenu.onmouseover = () => {
  isMouseOnMenu = true;
};

navMenu.onmouseleave = () => {
  setTimeout(() => {
    isMouseOnMenu = false;
    navMenu.classList.remove("active");
    isMenuActive = false;
  }, 66);
};